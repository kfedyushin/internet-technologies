document.addEventListener('DOMContentLoaded', () => {
  const app = new App();
  app.init();
});

class AppModel {
  static async getShelfs() {
    const shelfsRes = await fetch('http://localhost:4321/shelfs');
    return await shelfsRes.json();
  }

  static async addShelf(shelfName) {
    console.log(JSON.stringify({ shelfName }));
    const result = await fetch(
      'http://localhost:4321/shelfs',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ shelfName })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async addBook({
    shelfId,
    bookName,
    bookAuthor
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/books`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ bookName, bookAuthor })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async editBook({
    shelfId,
    bookId,
    newBookName,
    newBookAuthor
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/books/${bookId}`,
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ newBookName, newBookAuthor })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async deleteBook({
    shelfId,
    bookId
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${shelfId}/books/${bookId}`,
      {
        method: 'DELETE'
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }

  static async moveBook({
    fromShelfId,
    toShelfId,
    bookId
  }) {
    const result = await fetch(
      `http://localhost:4321/shelfs/${fromShelfId}`,
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ toShelfId, bookId })
      }
    );

    const resultData = await result.json();

    return result.status === 200
      ? resultData
      : Promise.reject(resultData);
  }
}

class App {
  constructor() {
    this.shelfs = [];
  }

  onEscapeKeydown = ({ key }) => {
    if (key === 'Escape') {
      const input = document.getElementById('add-shelf-input');
      input.style.display = 'none';
      input.value = '';

      document.getElementById('hw-shelf-add-shelf')
        .style.display = 'inherit';
    }
  };

  onInputKeydown = async ({ key, target }) => {
    if (key === 'Enter') {
      if (target.value) {
        await AppModel.addShelf(target.value);

        this.shelfs.push(
          new Shelf({
            tlName: target.value,
            tlID: `TL${this.shelfs.length}`,
            moveBook: this.moveBook
          })
        );

        this.shelfs[this.shelfs.length - 1].render();
      }
      
      target.style.display = 'none';
      target.value = '';

      document.getElementById('hw-shelf-add-shelf')
        .style.display = 'inherit';
    }
  };

  moveBook = async ({ bookID, direction }) => {
    let [
      tlIndex,
      bookIndex
    ] = bookID.split('-T');
    tlIndex = Number(tlIndex.split('TL')[1]);
    bookIndex = Number(bookIndex);
    const book = { bookName: this.shelfs[tlIndex].books[bookIndex].bookName,
      bookAuthor: this.shelfs[tlIndex].books[bookIndex].bookAuthor
    };
    const targetTlIndex = direction === 'left'
      ? tlIndex - 1
      : tlIndex + 1;

    try {
      await AppModel.moveBook({
        fromShelfId: tlIndex,
        toShelfId: targetTlIndex,
        bookId: bookIndex
      });

      this.shelfs[tlIndex].deleteBook(bookIndex);
      this.shelfs[targetTlIndex].addBook(book.bookName,book.bookAuthor);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  async init() {
    const shelfs = await AppModel.getShelfs();
    shelfs.forEach(({ shelfName, books }) => {
      const newShelf = new Shelf({
        tlName: shelfName,
        tlID: `TL${this.shelfs.length}`,
        moveBook: this.moveBook
      });
      books.forEach(book => newShelf.books.push(book));
      
      this.shelfs.push(newShelf);
      newShelf.render();
      newShelf.rerenderBooks();
    });

    document.getElementById('hw-shelf-add-shelf')
      .addEventListener(
        'click',
        (event) => {
          event.target.style.display = 'none';

          const input = document.getElementById('add-shelf-input');
          input.style.display = 'inherit';
          input.focus();
        }
      );

    document.addEventListener('keydown', this.onEscapeKeydown);

    document.getElementById('add-shelf-input')
      .addEventListener('keydown', this.onInputKeydown);

    document.querySelector('.toggle-switch input')
      .addEventListener(
        'change',
        ({ target: { checked } }) => {
          checked
            ? document.body.classList.add('dark-theme')
            : document.body.classList.remove('dark-theme');
        }
      );
  }
}

class Shelf {
  constructor({
    tlName,
    tlID,
    moveBook
  }) {
    this.tlName = tlName;
    this.tlID = tlID;
    this.books = [];
    this.moveBook = moveBook;
  }

  onAddBookButtonClick = async () => {
    const newBookName = prompt('Введите название книги:');
    const newBookAuthor = prompt('Введите автора книги:');

    if (!newBookName && !newBookAuthor) return;

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.addBook({
        shelfId,
        bookName: newBookName,
        bookAuthor: newBookAuthor
      });
      this.addBook(newBookName, newBookAuthor);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  addBook = (bookName, bookAuthor) => {
    const book = {
      bookName: bookName,
      bookAuthor: bookAuthor,
    };

    console.log(book);

    document.querySelector(`#${this.tlID} ul`)
      .appendChild(
        this.renderBook(
          `${this.tlID}-T${this.books.length}`,
          book
        )
      );

    this.books.push({bookName, bookAuthor});
  };

  onEditBook = async (bookID) => {
    const bookIndex = Number(bookID.split('-T')[1]);
    const oldBook = this.books[bookIndex];

    const newBookName = prompt('Введите новое название книги:', oldBook.bookName);
    const newBookAuthor= prompt('Введите нового автора книги:', oldBook.bookAuthor);

    if ((!newBookName || newBookName === oldBook.bookName) 
          && (!newBookAuthor || newBookAuthor === oldBook.bookAuthor)){
      return;
    }

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.editBook({
        shelfId,
        bookId: bookIndex,
        newBookName,
        newBookAuthor
      });

      this.books[bookIndex].bookName = newBookName;
      this.books[bookIndex].bookAuthor = newBookAuthor;
      document.querySelector(`#${bookID} span`)
        .innerHTML = `📖 ${newBookName} <br> 👤 ${newBookAuthor}`;
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  onDeleteBookButtonClick = async (bookID) => {
    const bookIndex = Number(bookID.split('-T')[1]);
    const bookName = this.books[bookIndex].bookName;

    if (!confirm(`Книга '${bookName}' будет удалена. Продолжить?`)) return;

    const shelfId = Number(this.tlID.split('TL')[1]);
    try {
      await AppModel.deleteBook({
        shelfId,
        bookId: bookIndex
      });

      this.deleteBook(bookIndex);
    } catch (error) {
      console.error('ERROR', error);
    }
  };

  deleteBook = (bookIndex) => {
    this.books.splice(bookIndex, 1);
    this.rerenderBooks();
  };

  rerenderBooks = () => {
    const shelf = document.querySelector(`#${this.tlID} ul`);
    shelf.innerHTML = '';

    this.books.forEach((book, bookIndex) => {
      shelf.appendChild(
        this.renderBook(
          `${this.tlID}-T${bookIndex}`,
          book
        )
      );
    });
  };

  renderBook = ( bookID, singleBook) => {
    const book = document.createElement('li');
    book.classList.add('hw-shelf-book');
    book.id = bookID;

    const span = document.createElement('span');
    span.classList.add('hw-shelf-book-text');
    span.innerHTML = `📖 ${singleBook.bookName} <br>  👤 ${singleBook.bookAuthor}`;
    book.appendChild(span);

    const controls = document.createElement('div');
    controls.classList.add('hw-shelf-book-controls');

    const upperRow = document.createElement('div');
    upperRow.classList.add('hw-shelf-book-controls-row');

    const leftArrow = document.createElement('button');
    leftArrow.type = 'button';
    leftArrow.classList.add(
      'hw-shelf-book-controls-button',
      'left-arrow'
    );
    leftArrow.addEventListener(
      'click',
      () => this.moveBook({ bookID, direction: 'left' })
    );
    upperRow.appendChild(leftArrow);

    const rightArrow = document.createElement('button');
    rightArrow.type = 'button';
    rightArrow.classList.add(
      'hw-shelf-book-controls-button',
      'right-arrow'
    );
    rightArrow.addEventListener(
      'click',
      () => this.moveBook({ bookID, direction: 'right' })
    );
    upperRow.appendChild(rightArrow);

    controls.appendChild(upperRow);

    const lowerRow = document.createElement('div');
    lowerRow.classList.add('hw-shelf-book-controls-row');

    const editButton = document.createElement('button');
    editButton.type = 'button';
    editButton.classList.add(
      'hw-shelf-book-controls-button',
      'edit-icon'
    );
    editButton.addEventListener('click', () => this.onEditBook(bookID));
    lowerRow.appendChild(editButton);

    const deleteButton = document.createElement('button');
    deleteButton.type = 'button';
    deleteButton.classList.add(
      'hw-shelf-book-controls-button',
      'delete-icon'
    );
    deleteButton.addEventListener('click', () => this.onDeleteBookButtonClick(bookID));
    lowerRow.appendChild(deleteButton);

    controls.appendChild(lowerRow);

    book.appendChild(controls);

    return book;
  };

  render() {
    const shelf = document.createElement('div');
    shelf.classList.add('hw-shelf');
    shelf.id = this.tlID;

    const header = document.createElement('header');
    header.classList.add('hw-shelf-header');
    header.innerHTML = this.tlName;
    shelf.appendChild(header);

    const list = document.createElement('ul');
    list.classList.add('hw-shelf-books');
    shelf.appendChild(list);

    const footer = document.createElement('footer');
    const button = document.createElement('button');
    button.type = 'button';
    button.classList.add('hw-shelf-add-book');
    button.innerHTML = 'Добавить книгу';
    button.addEventListener('click', this.onAddBookButtonClick);
    footer.appendChild(button);
    shelf.appendChild(footer);

    const container = document.querySelector('main');
    container.insertBefore(shelf, container.lastElementChild);
  }
}
