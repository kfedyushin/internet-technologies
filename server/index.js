import express from 'express';
import { resolve } from 'path';
import { __dirname } from './globals.js';
import { readData, writeData } from './fileUtils.js';

const app = express();

const hostname = 'localhost';
const port = 4321;

const shelfs = [];

// Middleware для формирования ответа в формате JSON
app.use(express.json());

// Middleware для логирования запросов
app.use((request, response, next) => {
  console.log(
    (new Date()).toISOString(),
    request.ip,
    request.method,
    request.originalUrl
  );

  next();
});

// Middleware для раздачи статики
app.use('/', express.static(
  resolve(__dirname, '..', 'public')
));

//---------------------------------------------------
// Роуты приложения

// Получение весех шкафов 
app.get('/shelfs', (request, response) => {
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json(shelfs);
});

// Создание нового шкафа
app.post('/shelfs', async (request, response) => {
  console.log(request);
  const { shelfName } = request.body;
  shelfs.push({
    shelfName,
    books: []
  });
  await writeData(shelfs);

  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Shelf'${shelfName}' was successfully created`
    });
});

// Создание новой книги
app.post('/shelfs/:shelfId/books', async (request, response) => {
  const { bookName, bookAuthor } = request.body;
  const shelfId = Number(request.params.shelfId);

  if (shelfId < 0 || shelfId >= shelfs.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${shelfId}`
      });
    return;
  }

  shelfs[shelfId].books.push({bookName, bookAuthor});
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Book '${bookName}' was successfully added in shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Изменение книги
app.put('/shelfs/:shelfId/books/:bookId', async (request, response) => {
  const bookName = request.body.newBookName ;
  const bookAuthor = request.body.newBookAuthor ;
  const shelfId = Number(request.params.shelfId);
  const bookId = Number(request.params.bookId);

  if (shelfId < 0 || shelfId >= shelfs.length
    || bookId < 0 || bookId >= shelfs[shelfId].books.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${
          shelfId} or book with id = ${bookId}`
      });
    return;
  }

  shelfs[shelfId].books[bookId] = { bookName, bookAuthor };
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `{Book №${bookId} was successfully edited in shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Удаление книги
app.delete('/shelfs/:shelfId/books/:bookId', async (request, response) => {
  const shelfId = Number(request.params.shelfId);
  const bookId = Number(request.params.bookId);

  if (shelfId < 0 || shelfId >= shelfs.length
    || bookId < 0 || bookId >= shelfs[shelfId].books.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${shelfId} or book with id = ${bookId}`
      });
    return;
  }

  const deletedBookName = shelfs[shelfId].books[bookId];
  shelfs[shelfId].books.splice(bookId, 1);
  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Book '${deletedBookName}' was successfully deleted from shelf '${shelfs[shelfId].shelfName}'`
    });
});

// Перенос книги из одного шкафа в другой
app.patch('/shelfs/:shelfId', async (request, response) => {
  const fromShelfId = Number(request.params.shelfId);
  const { toShelfId, bookId } = request.body;

  if (fromShelfId < 0 || fromShelfId >= shelfs.length
    || bookId < 0 || bookId >= shelfs[fromShelfId].books.length
    || toShelfId < 0 || fromShelfId >= shelfs.length) {
    response
      .setHeader('Content-Type', 'application/json')
      .status(404)
      .json({
        info: `There is no shelf with id = ${
          fromShelfId} of ${toShelfId} or book with id = ${bookId}`
      });
    return;
  }

  const movedBookName = shelfs[fromShelfId].books[bookId];

  shelfs[fromShelfId].books.splice(bookId, 1);
  shelfs[toShelfId].books.push(movedBookName);

  await writeData(shelfs);
  response
    .setHeader('Content-Type', 'application/json')
    .status(200)
    .json({
      info: `Book '${movedBookName}' was successfully moved from shelf '${shelfs[fromShelfId].shelfName}' to shelf '${
        shelfs[toShelfId].shelfName
      }'`
    });
}); 

//---------------------------------------------------

// Запуск сервера
app.listen(port, hostname, async (err) => {
  if (err) {
    console.error('Error: ', err);
    return;
  }

  console.log(`Out server started at http://${hostname}:${port}`);

  const shelfsFromFile = await readData();
  shelfsFromFile.forEach(({ shelfName, books }) => {
    shelfs.push({
      shelfName,
      books: [...books]
    });
  });
});
